import { LitElement, html, css } from 'lit';

class FichaPersona extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      yearsInCompany: { type: Number },
      personinfo: { type: String },
    };
  }

  constructor() {
    super();

    this.name = 'carlos';
    this.yearsInCompany = 5;

    //this._personInfoByYear(this.yearsInCompany);
  }

  updated(changedProperties) {
    console.log(changedProperties)
    /*changedProperties.forEach((oldValue, propName) => {
        console.log("111propiedad "+ propName + "ha cambiado el valor, el anterior era "+ oldValue);
    })
*/
    if(changedProperties.has("name")) {
        console.log("2222propiedad name cambia, el anterior es " + changedProperties.get('name')+ "el actual" + this.name)
    }
    if(changedProperties.has("yearsInCompany")) {
        this._personInfoByYear(this.yearsInCompany);
    }
  }


  render() {
    return html`
        <div>
            <label>Nombre completo</label>
            <input type="text" id="fname" value="${this.name}" @change="${this.updateName}"></input>
            <br/>
            <label>Años de empresa</label>
            <input type="text" value="${this.yearsInCompany}" @change="${this.updateYear}"></input>
            <br/>
            <label>Años de empresa</label>
            <input type="text" value="${this.personinfo}" disabled></input>
        </div>
        
    `;
  }

  updateName(e) {
      this.name = e.target.value;
  }
  updateYear(e) {
    const year = e.target.value;
    this.yearsInCompany = year; 
  }

  _personInfoByYear() {

    if(this.yearsInCompany>= 7) {
        this.personinfo = "lead";
    } else if (this.yearsInCompany >= 5) {
        this.personinfo = "senior";
    } else if (this.yearsInCompany >= 3) {
        this.personinfo = "team";
    }else {
        this.personinfo = "junior";
    }
  }
}

customElements.define('ficha-persona', FichaPersona);