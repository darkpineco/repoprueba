import { LitElement, html } from 'lit';

class PersonaFooter extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
  }


  render() {
    return html`
        <div>Persona Footer</div>
    `;
  }

}

customElements.define('persona-footer', PersonaFooter);