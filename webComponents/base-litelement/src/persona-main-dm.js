import { LitElement, html } from 'lit';

class PersonaMainDm extends LitElement {
  static get properties() {
    return {
        people: {type: Array},
    };
  }

  constructor() {
    super();

    this.people = [
        { name: "Ellen ripley", yearsInCompany: 10, photo: {src: "./img/persona.jpg", alt: "Ellen ripley"}, profile: "asd" },
        { name: "Bruce Banner", yearsInCompany: 2, photo: {src: "./img/persona.jpg", alt: "Bruce Banner" }, profile: "asd1"},
        { name: "Eowyn", yearsInCompany: 5, photo: {src: "./img/persona.jpg", alt: "Eowyn" } , profile: "asd2"},
        { name: "Ellen ripley1", yearsInCompany: 10, photo: {src: "./img/persona.jpg", alt: "Ellen ripley"}, profile: "asd3" },
        { name: "Bruce Banner1", yearsInCompany: 2, photo: {src: "./img/persona.jpg", alt: "Bruce Banner" }, profile: "asd4"},
        { name: "Eowyn1", yearsInCompany: 5, photo: {src: "./img/persona.jpg", alt: "Eowyn" }, profile: "asd5"},
    ]
  }

  updated(changedProperties) {
    if(changedProperties.has('people')){

      this.dispatchEvent(new CustomEvent(
        "load-people-dm",
        {
          detail: {
            peopleDm: {people: this.people}
          }
        }
      ));
    }
  }
}

customElements.define('persona-main-dm', PersonaMainDm);