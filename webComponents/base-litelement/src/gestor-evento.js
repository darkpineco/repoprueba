import { LitElement, html, css } from 'lit';

import "./emisor-evento.js"
import "./receptor-evento.js"

class GestorEvento extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
   
  }


  render() {
    return html`
        <h3>Gestor Evento</h3>
        <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
        <receptor-evento id="receiver"></receptor-evento>
    `;
  }

  processEvent(e) {
    console.log("Captura el evento del emisor");
    console.log(e)

    this.shadowRoot.getElementById("receiver").year = e.detail.year;
    this.shadowRoot.getElementById("receiver").course = e.detail.course;
  }
}

customElements.define('gestor-evento', GestorEvento);