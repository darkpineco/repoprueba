import { LitElement, html } from 'lit';
import "./persona-ficha-listado";
import "./persona-form";

class PersonaStats extends LitElement {
  static get properties() {
    return {
      people: {type: Array}
    };
  }

  constructor() {
    super();

    this.people = []
  }

  updated(changedProperties) {
    if(changedProperties.has('people')){

      this.dispatchEvent(new CustomEvent(
        "updated-people-stats",
        {
          detail: {
            peopleStats: this.gatherPeopleArrayInfo(this.people)
          }
        }
      ));
    }
  }

  gatherPeopleArrayInfo(people) {
    let peopleStats = {};

    peopleStats.numberOfPeople = people.length;

    return peopleStats;
  }
}

customElements.define('persona-stats', PersonaStats);