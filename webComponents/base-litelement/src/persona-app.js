import { LitElement, html } from 'lit';
import "./persona-header";
import "./persona-main";
import "./persona-footer";
import "./persona-sidebar";
import "./persona-stats";
import "./persona-main-dm";

class PersonaApp extends LitElement {
  static get properties() {
    return {
      people: {type: Array}
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
    if (changedProperties.has('people')) {
      this.shadowRoot.querySelector("persona-stats").people = this.people;
    }
  }


  render() {
    return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <persona-header></persona-header>
        <div class="row">
          <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
          <persona-main class="col-10" @people-updated="${this.peopleUpdated}"></persona-main>
        </div>
        <persona-footer></persona-footer>
        <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
        <persona-main-dm @load-people-dm="${this.loadPeopleDm}"></persona-main-dm>
    `;
  }

  newPerson(e) {
    this.shadowRoot.querySelector("persona-main").showPersonForm = true;
  }

  peopleStatsUpdated(e) {
    this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;
  }

  loadPeopleDm(e) {
    this.shadowRoot.querySelector("persona-main").people = e.detail.peopleDm.people;
  }

  peopleUpdated(e) {
    this.people = e.detail.people;
  }
}

customElements.define('persona-app', PersonaApp);