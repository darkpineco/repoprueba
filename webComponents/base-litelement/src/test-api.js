import { LitElement, html } from 'lit';

class TestApi extends LitElement {
  static get properties() {
    return {
        movies: {type: Array}
    };
  }

  constructor() {
    super();

    this.movies = [];
    this.getMovieData();
  }

  updated(changedProperties) {
  }


  render() {
    return html`
        <h1>Test API</h1>

        ${this.movies.map(
            movie => html`<div> La pelicula ${movie.title} directos -> ${movie.director}`
        )}
    `;
  }

  updated(changedProperties) {
    
  }

  getMovieData() {
      let xhr = new XMLHttpRequest();

      xhr.onload = () => {
          if (xhr.status === 200) {
            console.log("peticion completada correctamente");

            let APIResponse = JSON.parse(xhr.responseText);

            console.log(APIResponse);

            this.movies = APIResponse?.results;
          }
      }

      xhr.open("GET", "https://swapi.dev/api/films");
      xhr.send();

      console.log("FIN de movieData")
  }
}

customElements.define('test-api', TestApi);