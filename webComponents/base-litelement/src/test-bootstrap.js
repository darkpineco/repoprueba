import { LitElement, html, css } from 'lit';
import "./persona-ficha-listado";

class TestBootstrap extends LitElement {
  static get properties() {
    return {
    };
  }
  
  static get styles() {
    return css `
      .redbg {
        background-color: red;
      }
      
      .greenbg {
        background-color: green;
      }

      .bluebg {
        background-color: blue;
      }

      .greybg {
        background-color: grey;
      }`;
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
  }


  render() {
    return html `
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <h1>Test bootstrap</h1>
        <div class="row greybg">
          <div class="col-2 redbg">Col1</div>
          <div class="col-8 greenbg">Col2</div>
          <div class="col-2 bluebg">Col3</div>
        </div>
    `;
  }

}

customElements.define('test-bootstrap', TestBootstrap);