import { LitElement, html } from 'lit';

class PersonaForm extends LitElement {
  static get properties() {
    return {
      person: {type: Object},
      editingPerson: {type: Boolean}
    };
  }

  constructor() {
    super();

    this.resetFormData();
  }

  updated(changedProperties) {
  }


  render() {
    return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>
          <form>
            <div class="form-group">
              <label>Nombre completo</label>
              <input @input="${this.updateName}" type="text" class="form-control" placeholder="nombre completo" .value="${this.person.name}" ?disabled="${this.editingPerson}" />
            </div>

            <div class="form-group">
              <label>Perfil</label>
              <textarea @input="${this.updateProfile}" type="textArea" class="form-control" placeholder="Perfil" rows="5" .value="${this.person.profile}"></textarea>
            </div>

            <div class="form-group">
              <label>Años en la empresa</label>
              <input @input="${this.updateYearsInCompany}" type="text" class="form-control" placeholder="Años en la empresa" .value="${this.person.yearsInCompany}" />
            </div>

            <button class="btn btn-primary" @click="${this.goBack}"><b>Atras</b></button>
            <button class="btn btn-success" @click="${this.storePerson}"><b>Guardar</b></button>
          </form>
        </div>
    `;
  }

  goBack(e) {
    console.log("goBack");

    e.preventDefault();
    this.dispatchEvent(new CustomEvent("persona-form-close", {}));
    this.resetFormData();
  }

  storePerson(e) {
    console.log("storePerson");
    e.preventDefault();
    console.log(`propiedades name ${this.person.name}, profile ${this.person.profile}, years ${this.person.yearsInCompany}`)
    this.person.photo = {
      src: './img/persona.jpg',
      alt: 'Persona',
    };

    this.dispatchEvent(new CustomEvent("persona-form-store", {
      detail: {
        person: {
          name: this.person.name,
          profile: this.person.profile,
          yearsInCompany: this.person.yearsInCompany,
          photo: this.person.photo,
        },
        editingPerson: this.editingPerson
      }
    }))
  }

  updateName(e) {
    console.log("updateName")
    this.person.name = e.target.value
  }

  updateProfile(e) {
    console.log("updateProfile")
    this.person.profile = e.target.value
  }

  updateYearsInCompany(e) {
    console.log("updateYearsInCompany")
    this.person.yearsInCompany = e.target.value
  }

  resetFormData() {
    this.person = {
      name: '',
      yearsInCompany: '',
      profile: '',
    }
    this.editingPerson = false;
  }
}

customElements.define('persona-form', PersonaForm);