import { LitElement, html } from 'lit';

class PersonaHeader extends LitElement {
  static get properties() {
    return {
    };
  }

  constructor() {
    super();
  }

  updated(changedProperties) {
  }


  render() {
    return html`
        <div>Persona header</div>
    `;
  }

}

customElements.define('persona-header', PersonaHeader);