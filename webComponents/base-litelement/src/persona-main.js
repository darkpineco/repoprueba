import { LitElement, html } from 'lit';
import "./persona-ficha-listado";
import "./persona-form";

class PersonaMain extends LitElement {
  static get properties() {
    return {
        people: {type: Array},
        showPersonForm: {type: Boolean}
    };
  }

  constructor() {
    super();

    this.people= [];

    /*this.people = [
        { name: "Ellen ripley", yearsInCompany: 10, photo: {src: "./img/persona.jpg", alt: "Ellen ripley"}, profile: "asd" },
        { name: "Bruce Banner", yearsInCompany: 2, photo: {src: "./img/persona.jpg", alt: "Bruce Banner" }, profile: "asd1"},
        { name: "Eowyn", yearsInCompany: 5, photo: {src: "./img/persona.jpg", alt: "Eowyn" } , profile: "asd2"},
        { name: "Ellen ripley1", yearsInCompany: 10, photo: {src: "./img/persona.jpg", alt: "Ellen ripley"}, profile: "asd3" },
        { name: "Bruce Banner1", yearsInCompany: 2, photo: {src: "./img/persona.jpg", alt: "Bruce Banner" }, profile: "asd4"},
        { name: "Eowyn1", yearsInCompany: 5, photo: {src: "./img/persona.jpg", alt: "Eowyn" }, profile: "asd5"},
    ]*/
    this.showPersonForm = false;
  }

  render() {
    return html`
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <div>Persona Main</div>
        <div class="row" id="peopleList">
          <main class="row row-cols-1 row-cols-sm-4">
              ${this.people.map(person => html `<persona-ficha-listado
                                                  fname="${person.name}"
                                                  yearsInCompany="${person.yearsInCompany}"
                                                  .photo="${person.photo}"
                                                  profile="${person.profile}"
                                                  @delete-person="${this.deletePerson}"
                                                  @info-person="${this.infoPerson}">
                                              </persona-ficha-listado>`)}
          </main>
        </div>
        <div class="row">
          <persona-form
            id="personaForm"
            class="d-none border rounded border-primary"
            @persona-form-close="${this.goBack}"
            @persona-form-store="${this.personaFormStore}"></persona-form>
        </div>
    `;
  }

  updated(changedProperties) {
    console.log(`updated de main ${changedProperties}`);

    if(changedProperties.has("showPersonForm")) {
      console.log(`ha cambiado el valor de showpersonForms a ${this.showPersonForm}`)
      
      if(this.showPersonForm) {
        this.showPersonFormData();
      }else {
        this.showPersonList();
      }
    }

    if (changedProperties.has("people")) {
      this.dispatchEvent( new CustomEvent(
        "people-updated",
        {
          detail: {
            people: this.people
          }
        }
      ))
    }
  }

  deletePerson(e) {
    console.log("deletePerson en persona-main");
    console.log("Se  va a borrar la persona de nombre "+ e.detail.name);

    this.people = this.people.filter(
      person => person.name != e.detail.name
    );
  }

  showPersonList() {
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    this.shadowRoot.getElementById("personaForm").classList.add("d-none");
  }

  showPersonFormData() {
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    this.shadowRoot.getElementById("personaForm").classList.remove("d-none");
  }

  goBack() {
    this.showPersonForm = false;
  }

  personaFormStore(e) {
    console.log("Store en main");

    if(e.detail.editingPerson === true) {
      // se va a actualizar a la persona
      /*let indexOfPerson = this.people.findIndex(person => person.name === e.detail.person.name) 

      if(indexOfPerson >= 0) {
        this.people[indexOfPerson] = e.detail.person;
      }*/

      this.people = this.people.map((person) => person.name === e.detail.person.name ? person = e.detail.person : person );
    } else {
      //this.people.push(e.detail.person)

      this.people = [...this.people, e.detail.person];
    }
 

    console.log(e.detail.person);
    this.showPersonForm = false;
  }

  infoPerson(e) {
    console.log("ifno Person");

    console.log(e.detail.name);

    let chosenPerson = this.people.find((person) => person.name === e.detail.name);

    let person = {};

    person.name = chosenPerson.name;
    person.profile = chosenPerson.profile;
    person.yearsInCompany = chosenPerson.yearsInCompany;

    this.shadowRoot.getElementById("personaForm").person = person;
    this.shadowRoot.getElementById("personaForm").editingPerson = true;
    this.showPersonForm = true;
  }
}

customElements.define('persona-main', PersonaMain);